package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ComparatorNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) throw new ComparatorNotFoundException();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task findOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(final String name, final String nameNew, final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task setTaskStatusById(final String id, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByName(final String name, final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByIndex(final Integer index, final Status status) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public String getIdByIndex(Integer index) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        return taskRepository.getIdByIndex(index);
    }

    @Override
    public int size() {
        return taskRepository.size();
    }

    @Override
    public boolean existsById(String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByName(String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.existsByName(name);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        if (!checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        taskRepository.removeOneByIndex(index);
    }

    @Override
    public void removeOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        taskRepository.removeOneByName(name);
    }

    @Override
    public void removeOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeOneById(id);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
