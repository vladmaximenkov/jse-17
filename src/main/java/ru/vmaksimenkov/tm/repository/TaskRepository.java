package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task bindTaskPyProjectId(final String projectId, final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public String getIdByIndex(int index) {
        return list.get(index).getId();
    }

    @Override
    public boolean existsById(String id) {
        for (final Task task : list) {
            if (id.equals(task.getId())) return true;
        }
        return false;
    }

    @Override
    public boolean existsByProjectId(final String projectId) {
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId())) return true;
        }
        return false;
    }

    @Override
    public boolean existsByName(final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName())) return true;
        }
        return false;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        for (int i = list.size(); i-- > 0; ) {
            if (projectId.equals(list.get(i).getProjectId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public void removeAllBinded() {
        for (int i = list.size(); i-- > 0; ) {
            if (!isEmpty(list.get(i).getProjectId())) {
                list.remove(i);
            }
        }
    }

    @Override
    public void removeOneById(final String id) {
        remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        remove(findOneByIndex(index));
    }

    @Override
    public void removeOneByName(final String name) {
        remove(findOneByName(name));
    }

    @Override
    public void add(Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

}
