package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIdSetStatusCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-id";
    }

    @Override
    public String description() {
        return "Set project status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsById(id)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().setProjectStatusById(id, Status.getStatus(TerminalUtil.nextLine()));
    }

}
