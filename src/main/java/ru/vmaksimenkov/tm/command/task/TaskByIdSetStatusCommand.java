package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskByIdSetStatusCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-id";
    }

    @Override
    public String description() {
        return "Set task status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsById(id)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().setTaskStatusById(id, Status.getStatus(TerminalUtil.nextLine()));
    }

}
