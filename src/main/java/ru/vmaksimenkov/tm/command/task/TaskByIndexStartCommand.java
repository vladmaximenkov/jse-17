package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIndexStartCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "Start task by index";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getTaskService().startTaskByIndex(TerminalUtil.nextNumber() - 1);
    }

}
