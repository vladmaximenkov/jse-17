package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    String getIdByName(String name);

    String getIdByIndex(Integer index);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    int size();

    boolean existsById(String id);

    boolean existsByName(String name);

    void remove(Project project);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    void removeOneById(String id);

    void add(Project project);

    void clear();

}
