package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task bindTaskPyProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    String getIdByIndex(int index);

    int size();

    boolean existsById(String id);

    boolean existsByProjectId(String projectId);

    boolean existsByName(String name);

    void add(Task task);

    void remove(Task task);

    void removeOneByName(String name);

    void removeOneByIndex(Integer index);

    void removeOneById(String id);

    void removeAllBinded();

    void removeAllByProjectId(String projectId);

    void clear();

}
