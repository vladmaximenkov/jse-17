package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByName(String name, String nameNew, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project setProjectStatusById(String id, Status status);

    Project setProjectStatusByName(String name, Status status);

    Project setProjectStatusByIndex(Integer index, Status status);

    int size();

    boolean existsById(String id);

    boolean existsByName(String name);

    void add(Project project);

    void remove(Project project);

    void removeOneById(String id);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    void clear();

}
