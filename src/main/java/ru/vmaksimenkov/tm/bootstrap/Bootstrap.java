package ru.vmaksimenkov.tm.bootstrap;

import ru.vmaksimenkov.tm.api.repository.ICommandRepository;
import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.*;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.command.project.*;
import ru.vmaksimenkov.tm.command.system.*;
import ru.vmaksimenkov.tm.command.task.*;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.system.UnknownCommandException;
import ru.vmaksimenkov.tm.repository.CommandRepository;
import ru.vmaksimenkov.tm.repository.ProjectRepository;
import ru.vmaksimenkov.tm.repository.TaskRepository;
import ru.vmaksimenkov.tm.service.*;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIdSetStatusCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIdViewCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByIndexSetStatusCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByIndexViewCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByNameSetStatusCommand());
        registry(new ProjectByNameStartCommand());
        registry(new ProjectByNameUpdateCommand());
        registry(new ProjectByNameViewCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectListSortCommand());

        registry(new TaskByIdFinishCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIdSetStatusCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIdUnbindCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIdViewCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByIndexSetStatusCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByIndexViewCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByNameSetStatusCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByNameUpdateCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByProjectIdBindCommand());
        registry(new TaskByProjectIdListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskListSortCommand());

        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
    }

    private void initData() {
        projectService.add("1 DEMO PROJECT 1", "DESCRIPTION PROJECT 1 CP").setStatus(Status.COMPLETE);
        projectService.add("DEMO PROJECT 2", "DESCRIPTION PROJECT 2 IP").setStatus(Status.IN_PROGRESS);
        projectService.add("B DEMO PROJECT 3", "DESCRIPTION PROJECT 3 IP").setStatus(Status.IN_PROGRESS);
        projectService.add("DEMO PROJECT 4", "DESCRIPTION PROJECT 4 NS").setStatus(Status.NOT_STARTED);
        projectService.add("C DEMO PROJECT 5", "DESCRIPTION PROJECT 5 CP").setStatus(Status.COMPLETE);
        projectService.add("A DEMO PROJECT 6", "DESCRIPTION PROJECT 6 NS").setStatus(Status.NOT_STARTED);

        taskService.add("DEMO TASK 1", "DESCRIPTION TASK CP").setStatus(Status.COMPLETE);
        taskService.add("C DEMO TASK 2", "DESCRIPTION TASK NS").setStatus(Status.NOT_STARTED);
        taskService.add("DEMO TASK 3", "DESCRIPTION TASK IP").setStatus(Status.IN_PROGRESS);
        taskService.add("B DEMO TASK 4", "DESCRIPTION TASK NS").setStatus(Status.NOT_STARTED);
        taskService.add("DEMO TASK 5", "DESCRIPTION TASK IP").setStatus(Status.IN_PROGRESS);
        taskService.add("A DEMO TASK 6", "DESCRIPTION TASK NS").setStatus(Status.NOT_STARTED);

        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(5).getId(), taskService.findOneByIndex(5).getId());
        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(2).getId(), taskService.findOneByIndex(2).getId());
        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(4).getId(), taskService.findOneByIndex(1).getId());
        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(0).getId(), taskService.findOneByIndex(0).getId());
        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(0).getId(), taskService.findOneByIndex(3).getId());
        projectTaskService.bindTaskByProjectId(projectService.findOneByIndex(0).getId(), taskService.findOneByIndex(4).getId());
    }

    public void run(final String... args) {
        loggerService.debug("test");
        loggerService.info("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("Enter command: ");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        if (isEmpty(arg)) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String cmd) {
        if (isEmpty(cmd)) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
